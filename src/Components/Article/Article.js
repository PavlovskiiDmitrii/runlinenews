import React from 'react';
import PropTypes from 'prop-types';

import './Article.scss'

class Article extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
      }

    render() {
      return (
        <a target="black" href={this.props.href} className={`articles`}>
            <div className={`articles__sourse`}>
                {this.props.sourse}
            </div> 
            <div className={`articles__text`}>
                {this.props.title}
            </div>
        </a>     
      )
    }
  }


Article.propTypes = {
};

Article.defaultProps = {
};
 
export default Article;