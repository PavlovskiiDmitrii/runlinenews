import React from "react";


import './NewsLine.scss';

class NewsLine extends React.Component {
    constructor(props) {
      super(props);

      this.myRef = React.createRef();
    };    

    componentDidUpdate(prevProps, prevState) {
      console.log('componentDidUpdate__NewsLine')
      if (this.props.children.length && !prevProps.children.length) {
        console.log(+this.myRef.current.getBoundingClientRect().width.toFixed(0))
        this.props.onLoadWidth(+this.myRef.current.getBoundingClientRect().width.toFixed(0) + this.props.children.length*30)
      }
    }
      
    render() {
        return (
            <div ref={this.myRef} className="newsLine">
                {this.props.children}
            </div>
        );
    }
    
  };

export default NewsLine;
