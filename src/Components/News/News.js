import React from 'react';
import NewsLine from '../NewsLine/NewsLine';
import Article from '../Article/Article';
import PropTypes from 'prop-types';

import './News.scss'

class News extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          widthNewsWrap: 0
        };

        this.onLoadWidth = this.onLoadWidth.bind(this);
      };

      componentDidUpdate(prevProps, prevState) {
        console.log('componentDidUpdate__News')
        console.log('                   ')
      }

      onLoadWidth(width) {
        this.setState({
          widthNewsWrap: width
        })
      }

    render() {
      return (
        <div className={`news`}>
            <div className={`news__wrap`} style={ this.state.widthNewsWrap ? {width: `${this.state.widthNewsWrap}px`} : {}}>
              <NewsLine onLoadWidth={this.onLoadWidth}>
                {
                  this.props.articles &&
                  this.props.articles.map((article, i) => (
                    <Article 
                      href={article.url} 
                      sourse={article.source.name} 
                      title={article.title} 
                      key={article.title}
                    />
                  ))
                }
              </NewsLine>
            </div>
        </div>
      )
    }
  }

News.propTypes = {
};

News.defaultProps = {
};
 
export default News;