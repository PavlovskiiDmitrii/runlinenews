import React from "react";
import News from '../News/News';

import { connect } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { newsActions } from '../../redux/actions/index';

import './App.scss';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            articles: [],
        };
      };

    componentDidMount() {
        this.props.fetchNews();
    }

    componentDidUpdate(prevProps, prevState) {
        console.log('componentDidUpdate__App')
        if (this.props.articles.length && !prevProps.articles.length) {
            this.setState({
                articles: this.props.articles
            })
        }
    }

    render() {
        return (
           
                <BrowserRouter>
                    <div className="app-wrap">
                        <div style={{
                            width: '500px',
                            height: '100px',
                            border: '1px solid red',
                            padding: '10px'
                        }}>
                            <News articles={this.state.articles}/>
                        </div>
                    </div>
                </BrowserRouter>
            
        )
    }
}

export default connect(
    ({
        news
    }) => ({
        articles: news.items
    }),
    {...newsActions },
  )(App);
  