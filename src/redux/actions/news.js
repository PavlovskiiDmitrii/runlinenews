import { news } from '../../utils/api';

const Actions = {
    setNews: (items) => ({
        type: 'NEWS:SET_ITEMS',
        payload: items
    }),
    fetchNews: () => dispatch => {
        news.getAll().then(({data}) => {
            dispatch(Actions.setNews(data.articles));
        })
    }
}

export default Actions;
