const initialState = {
    items: []
}

export default (state = initialState, news) => {

    switch (news.type) {
        case 'NEWS:SET_ITEMS':
            return {
                ...state,
                items: news.payload
            };
        default:
            return state
    }
}
