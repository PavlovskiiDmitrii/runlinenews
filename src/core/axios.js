import axios from 'axios';

axios.defaults.baseURL = 'http://newsapi.org/v2/';

export default axios;
