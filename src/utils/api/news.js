import {axios} from "../../core/";

export default {
    getAll: () => axios.get("top-headlines?country=ru&apiKey=e4f139fea2fd41feb526106f9a5d746c")
};
